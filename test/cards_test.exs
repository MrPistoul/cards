defmodule CardsTest do
  use ExUnit.Case
  doctest Cards


## assert pour vérifier qu’une expression est correcte
  test "create_deck makes 20 cards" do
    deck_length = length(Cards.create_deck)
    assert deck_length == 20
  end

## refute pour vous assurer qu’une expression est fausse
  test "shuffling a deck randomizes it" do
    deck = Cards.create_deck
    refute deck == Cards.shuffle(deck)
  end
end
