defmodule Cards do
  @moduledoc """
    Provides methods for creating and handling a deck of cards
    """
                            ##moduledoc utilisé pour document le module grace a :ex_doc dans mix.exs (comme composer)
                            ## en ligne de commande mix docs genere les fichiers html de documentation


        @doc"""
        Return a list of strings representing a deck of playing card
        """
    def create_deck do
       values =  ["Ace","Two", "Thee", "Four", "Five"]
        suits =  ["Spades", "Clubs", "Hearts", "Diamonds"]



## Truc de OUF !! alors il fait l'equivalent du foreach avec les deux valeurs en meme temps -- 2eme solution  LA MEILLEURE !
## et rend toutes les combinaison possible
            for suit <- suits , value <- values do
                "#{value} of #{suit}"
            end

##                                                          rend un array contenant la liste des cartes  -- 1 solution
#                                                           Foreach ==>
#            cards =  for value <- values do
#                 #Foreach ==>
#                 for suit <- suits do
#                   "#{value} of #{suit}"
#                 end
#            end
#            List.flatten(cards)
    end

    def shuffle(deck) do
        Enum.shuffle(deck)
    end

                            ## Pour les examples attention  au saut de ligne et au 3 tabulations a faire pour le code
                            ## auw espace entre iex> et le code , atout en fait !!!!
                            ## la commande mix test va tester les fonction en exemple dans la doc
                            ## truc de fou !!
@doc"""
Determines whether a deck contains a given card

## Examples

            iex> deck = Cards.create_deck
            iex> Cards.contains?(deck, "Two of Spades")
            true
"""
    def contains?(deck, card) do
        Enum.member?(deck, card)
    end

                                    ## Truc super important Elixir se base sur du pattern matching
                                    ## celà induit =>
                                    ## color = ["red"]   color est égale a la liste
                                    ## [color] = ['red'] color  est égale la string "red"  ==>
                                    ## [color1 , color2] = ['red', 'blue'] => color2 = "blue"" etc ..
                                    # Attention on peut hardCoder ex :  ["red", color] = ["red", "blue"]
                                    # red est une string et non une variable (dans l'initialisation) est doit
                                    # donc etre egale à "red"  dans la declaration
                                    # ["red", color] = ["green", "blue"]  ==> error  ====> PATTERN MATCHING


@doc"""
Divides a deck into a hand and the remainder of the deck.
The `hand_size` argument indicates how many should be in the hand.

## Examples

            iex> deck = Cards.create_deck
            iex> {hand,deck} = Cards.deal(deck,1)
            iex> hand
            ["Ace of Spades"]

"""

    def deal(deck, hand_size) do
      Enum.split(deck, hand_size)
    end
 ## la function deal va me rendre un tuple {} de 2 array {[],[]}et je ne pourrais pas acceder a ma main en faisant =>
                                    ## main = Cards.deal(deck,5)
                                    ## main[0] => ne marche pas  !!!!!!!!!
                                    ## il faut => [main , reste] = Cards.deal(deck,5)
                                    ## la je pourrais avoir ma main avec la variable main

    def save(deck, filename) do
      binary = :erlang.term_to_binary(deck)
      File.write(filename, binary)
    end

#    def load(filename) do                                      # Première Methode
#      {status, binary} = File.read(filename)
#      case status do
#        :ok     ->  :erlang.binary_to_term(binary)
#        :error  ->  "No file"
#      end
#    end

                                                                # Second method
                            # en exploitant le pattern matching , si le premier element du tuple
                            # de la reponse de File.read(filename) est :ok => ok ou :error

    def load(filename) do
          case File.read(filename) do
            {:ok, binary}     ->  :erlang.binary_to_term(binary)
                            # Dans le cas de l'erreur nous avons besoin d'une variable pour notre message d'erreur (si l'on veut le personaliser)
                            # si l'on met {:error, reason}, un message d'alerte(non bloquant) à la compilation nous indique que reason n'est pas utilisé
                            # ce qui est vrai, car on ne l'utilise pas directement dans le code.
                            # si l'on a besoin de declaré une variable que l'on n'utilisera pas on met un _ devant
            {:error, _reason}  ->  "No file to read"
          end
    end

    def create_hand(hand_size) do
#      deck = Cards.create_deck
#      deck = Cards.shuffle(deck)
#      hand = Cards.deal(deck, hand_size)                   # 1er Method

      Cards.create_deck                                   # 2eme Method
      |> Cards.shuffle                                    # Encore un truc de ouf ! !
      |> Cards.deal(hand_size)                            # le pipe enchaine les fonction du coup , plus de variable deck
                                                          # à declarer , LE RETOUR DE CHAQUE FONCTION EST INJECTé dans
    end                                                   # en tant que 1er argument de la fonction suivante
end                                                       # donc : Cards.create.hand(5) 5 est hand_size

